[require]
GLSL >= 1.50

[vertex shader]
#version 150
#define GSK_GL3 1
#ifndef GSK_LEGACY
precision highp float;
#endif

#if defined(GSK_GLES) || defined(GSK_LEGACY)
#define _OUT_ varying
#define _IN_ varying
#define _GSK_ROUNDED_RECT_UNIFORM_ vec4[3]
#else
#define _OUT_ out
#define _IN_ in
#define _GSK_ROUNDED_RECT_UNIFORM_ GskRoundedRect
#endif


struct GskRoundedRect
{
  vec4 bounds; // Top left and bottom right
  // Look, arrays can't be in structs if you want to return the struct
  // from a function in gles or whatever. Just kill me.
  vec4 corner_points1; // xy = top left, zw = top right
  vec4 corner_points2; // xy = bottom right, zw = bottom left
};

// Transform from a C GskRoundedRect to what we need.
GskRoundedRect
gsk_create_rect(vec4[3] data)
{
  vec4 bounds = vec4(data[0].xy, data[0].xy + data[0].zw);

  vec4 corner_points1 = vec4(bounds.xy + data[1].xy,
                             bounds.zy + vec2(data[1].zw * vec2(-1, 1)));
  vec4 corner_points2 = vec4(bounds.zw + (data[2].xy * vec2(-1, -1)),
                             bounds.xw + vec2(data[2].zw * vec2(1, -1)));

  return GskRoundedRect(bounds, corner_points1, corner_points2);
}

vec4
gsk_get_bounds(vec4[3] data)
{
  return vec4(data[0].xy, data[0].xy + data[0].zw);
}

vec4 gsk_premultiply(vec4 c) {
  return vec4(c.rgb * c.a, c.a);
}

vec4 gsk_scaled_premultiply(vec4 c, float s) {
  // Fast version of gsk_premultiply(c) * s
  // 4 muls instead of 7
  float a = s * c.a;

  return vec4(c.rgb * a, a);
}
uniform mat4 u_projection;
uniform mat4 u_modelview;
uniform float u_alpha;

#if defined(GSK_GLES) || defined(GSK_LEGACY)
attribute vec2 aPosition;
attribute vec2 aUv;
attribute vec4 aColor;
attribute vec4 aColor2;
_OUT_ vec2 vUv;
#else
_IN_ vec2 aPosition;
_IN_ vec2 aUv;
_IN_ vec4 aColor;
_IN_ vec4 aColor2;
_OUT_ vec2 vUv;
#endif

// amount is: top, right, bottom, left
GskRoundedRect
gsk_rounded_rect_shrink (GskRoundedRect r, vec4 amount)
{
  vec4 new_bounds = r.bounds + vec4(1.0,1.0,-1.0,-1.0) * amount.wxyz;
  vec4 new_corner_points1 = r.corner_points1;
  vec4 new_corner_points2 = r.corner_points2;

  if (r.corner_points1.xy == r.bounds.xy) new_corner_points1.xy = new_bounds.xy;
  if (r.corner_points1.zw == r.bounds.zy) new_corner_points1.zw = new_bounds.zy;
  if (r.corner_points2.xy == r.bounds.zw) new_corner_points2.xy = new_bounds.zw;
  if (r.corner_points2.zw == r.bounds.xw) new_corner_points2.zw = new_bounds.xw;

  return GskRoundedRect (new_bounds, new_corner_points1, new_corner_points2);
}

void
gsk_rounded_rect_offset(inout GskRoundedRect r, vec2 offset)
{
  r.bounds.xy += offset;
  r.bounds.zw += offset;
  r.corner_points1.xy += offset;
  r.corner_points1.zw += offset;
  r.corner_points2.xy += offset;
  r.corner_points2.zw += offset;
}

void gsk_rounded_rect_transform(inout GskRoundedRect r, mat4 mat)
{
  r.bounds.xy = (mat * vec4(r.bounds.xy, 0.0, 1.0)).xy;
  r.bounds.zw = (mat * vec4(r.bounds.zw, 0.0, 1.0)).xy;

  r.corner_points1.xy = (mat * vec4(r.corner_points1.xy, 0.0, 1.0)).xy;
  r.corner_points1.zw = (mat * vec4(r.corner_points1.zw, 0.0, 1.0)).xy;

  r.corner_points2.xy = (mat * vec4(r.corner_points2.xy, 0.0, 1.0)).xy;
  r.corner_points2.zw = (mat * vec4(r.corner_points2.zw, 0.0, 1.0)).xy;
}

#if defined(GSK_LEGACY)
// Can't have out or inout array parameters...
#define gsk_rounded_rect_encode(r, uni) uni[0] = r.bounds; uni[1] = r.corner_points1; uni[2] = r.corner_points2;
#else
void gsk_rounded_rect_encode(GskRoundedRect r, out _GSK_ROUNDED_RECT_UNIFORM_ out_r)
{
#if defined(GSK_GLES)
  out_r[0] = r.bounds;
  out_r[1] = r.corner_points1;
  out_r[2] = r.corner_points2;
#else
  out_r = r;
#endif
}

#endif

// blend.glsl

void main() {
  gl_Position = u_projection * u_modelview * vec4(aPosition, 0.0, 1.0);

  vUv = vec2(aUv.x, aUv.y);
}

// FRAGMENT_SHADER:
[fragment shader]
#version 150
#define GSK_GL3 1
#ifndef GSK_LEGACY
precision highp float;
#endif

#if defined(GSK_GLES) || defined(GSK_LEGACY)
#define _OUT_ varying
#define _IN_ varying
#define _GSK_ROUNDED_RECT_UNIFORM_ vec4[3]
#else
#define _OUT_ out
#define _IN_ in
#define _GSK_ROUNDED_RECT_UNIFORM_ GskRoundedRect
#endif


struct GskRoundedRect
{
  vec4 bounds; // Top left and bottom right
  // Look, arrays can't be in structs if you want to return the struct
  // from a function in gles or whatever. Just kill me.
  vec4 corner_points1; // xy = top left, zw = top right
  vec4 corner_points2; // xy = bottom right, zw = bottom left
};

// Transform from a C GskRoundedRect to what we need.
GskRoundedRect
gsk_create_rect(vec4[3] data)
{
  vec4 bounds = vec4(data[0].xy, data[0].xy + data[0].zw);

  vec4 corner_points1 = vec4(bounds.xy + data[1].xy,
                             bounds.zy + vec2(data[1].zw * vec2(-1, 1)));
  vec4 corner_points2 = vec4(bounds.zw + (data[2].xy * vec2(-1, -1)),
                             bounds.xw + vec2(data[2].zw * vec2(1, -1)));

  return GskRoundedRect(bounds, corner_points1, corner_points2);
}

vec4
gsk_get_bounds(vec4[3] data)
{
  return vec4(data[0].xy, data[0].xy + data[0].zw);
}

vec4 gsk_premultiply(vec4 c) {
  return vec4(c.rgb * c.a, c.a);
}

vec4 gsk_scaled_premultiply(vec4 c, float s) {
  // Fast version of gsk_premultiply(c) * s
  // 4 muls instead of 7
  float a = s * c.a;

  return vec4(c.rgb * a, a);
}
uniform sampler2D u_source;
uniform mat4 u_projection;
uniform mat4 u_modelview;
uniform float u_alpha;
uniform vec4 u_viewport;
uniform vec4[3] u_clip_rect;

#if defined(GSK_LEGACY)
_OUT_ vec4 outputColor;
#elif !defined(GSK_GLES)
_OUT_ vec4 outputColor;
#endif

_IN_ vec2 vUv;


GskRoundedRect gsk_decode_rect(_GSK_ROUNDED_RECT_UNIFORM_ r)
{
#if defined(GSK_GLES) || defined(GSK_LEGACY)
  return GskRoundedRect(r[0], r[1], r[2]);
#else
  return r;
#endif
}

float
gsk_ellipsis_dist (vec2 p, vec2 radius)
{
  if (radius == vec2(0, 0))
    return 0.0;

  vec2 p0 = p / radius;
  vec2 p1 = 2.0 * p0 / radius;

  return (dot(p0, p0) - 1.0) / length (p1);
}

float
gsk_ellipsis_coverage (vec2 point, vec2 center, vec2 radius)
{
  float d = gsk_ellipsis_dist (point - center, radius);
  return clamp (0.5 - d, 0.0, 1.0);
}

float
gsk_rounded_rect_coverage (GskRoundedRect r, vec2 p)
{
  if (p.x < r.bounds.x || p.y < r.bounds.y ||
      p.x >= r.bounds.z || p.y >= r.bounds.w)
    return 0.0;

  vec2 ref_tl = r.corner_points1.xy;
  vec2 ref_tr = r.corner_points1.zw;
  vec2 ref_br = r.corner_points2.xy;
  vec2 ref_bl = r.corner_points2.zw;

  if (p.x >= ref_tl.x && p.x >= ref_bl.x &&
      p.x <= ref_tr.x && p.x <= ref_br.x)
    return 1.0;

  if (p.y >= ref_tl.y && p.y >= ref_tr.y &&
      p.y <= ref_bl.y && p.y <= ref_br.y)
    return 1.0;

  vec2 rad_tl = r.corner_points1.xy - r.bounds.xy;
  vec2 rad_tr = r.corner_points1.zw - r.bounds.zy;
  vec2 rad_br = r.corner_points2.xy - r.bounds.zw;
  vec2 rad_bl = r.corner_points2.zw - r.bounds.xw;

  float d_tl = gsk_ellipsis_coverage(p, ref_tl, rad_tl);
  float d_tr = gsk_ellipsis_coverage(p, ref_tr, rad_tr);
  float d_br = gsk_ellipsis_coverage(p, ref_br, rad_br);
  float d_bl = gsk_ellipsis_coverage(p, ref_bl, rad_bl);

  vec4 corner_coverages = 1.0 - vec4(d_tl, d_tr, d_br, d_bl);

  bvec4 is_out = bvec4(p.x < ref_tl.x && p.y < ref_tl.y,
                       p.x > ref_tr.x && p.y < ref_tr.y,
                       p.x > ref_br.x && p.y > ref_br.y,
                       p.x < ref_bl.x && p.y > ref_bl.y);

  return 1.0 - dot(vec4(is_out), corner_coverages);
}

float
gsk_rect_coverage (vec4 r, vec2 p)
{
  if (p.x < r.x || p.y < r.y ||
      p.x >= r.z || p.y >= r.w)
    return 0.0;

  return 1.0;
}

vec4 GskTexture(sampler2D sampler, vec2 texCoords) {
#if defined(GSK_GLES) || defined(GSK_LEGACY)
  return texture2D(sampler, texCoords);
#else
  return texture(sampler, texCoords);
#endif
}

#ifdef GSK_GL3
layout(origin_upper_left) in vec4 gl_FragCoord;
#endif

vec2 gsk_get_frag_coord() {
  vec2 fc = gl_FragCoord.xy;

#ifdef GSK_GL3
  fc += u_viewport.xy;
#else
  fc.x += u_viewport.x;
  fc.y = (u_viewport.y + u_viewport.w) - fc.y;
#endif

  return fc;
}

void gskSetOutputColor(vec4 color) {
  vec4 result;

#if defined(NO_CLIP)
  result = color;
#elif defined(RECT_CLIP)
  float coverage = gsk_rect_coverage(gsk_get_bounds(u_clip_rect),
                                     gsk_get_frag_coord());
  result = color * coverage;
#else
  float coverage = gsk_rounded_rect_coverage(gsk_create_rect(u_clip_rect),
                                             gsk_get_frag_coord());
  result = color * coverage;
#endif

#if defined(GSK_GLES) || defined(GSK_LEGACY)
  gl_FragColor = result;
#else
  outputColor = result;
#endif
}

void gskSetScaledOutputColor(vec4 color, float alpha) {
  vec4 result;

#if defined(NO_CLIP)
  result = color * alpha;
#elif defined(RECT_CLIP)
  float coverage = gsk_rect_coverage(gsk_get_bounds(u_clip_rect),
                                     gsk_get_frag_coord());
  result = color * (alpha * coverage);
#else
  float coverage = gsk_rounded_rect_coverage(gsk_create_rect(u_clip_rect),
                                             gsk_get_frag_coord());
  result = color * (alpha * coverage);
#endif

#if defined(GSK_GLES) || defined(GSK_LEGACY)
  gl_FragColor = result;
#else
  outputColor = result;
#endif
}

// blend.glsl

uniform int u_mode;
uniform sampler2D u_source2;

float
combine (float source, float backdrop)
{
  return source + backdrop * (1.0 - source);
}

vec4
composite (vec4 Cs, vec4 Cb, vec3 B)
{
  float ao = Cs.a + Cb.a * (1.0 - Cs.a);
  vec3 Co = (Cs.a*(1.0 - Cb.a)*Cs.rgb + Cs.a*Cb.a*B + (1.0 - Cs.a)*Cb.a*Cb.rgb) / ao;
  return vec4(Co, ao);
}

vec4
normal (vec4 Cs, vec4 Cb)
{
  return composite (Cs, Cb, Cs.rgb);
}

vec4
multiply (vec4 Cs, vec4 Cb)
{
  return composite (Cs, Cb, Cs.rgb * Cb.rgb);
}

vec4
difference (vec4 Cs, vec4 Cb)
{
  return composite (Cs, Cb, abs(Cs.rgb - Cb.rgb));
}

vec4
screen (vec4 Cs, vec4 Cb)
{
  return composite (Cs, Cb, Cs.rgb + Cb.rgb - Cs.rgb * Cb.rgb);
}

float
hard_light (float source, float backdrop)
{
  if (source <= 0.5)
    return 2.0 * backdrop * source;
  else
    return 2.0 * (backdrop + source - backdrop * source) - 1.0;
}

vec4
hard_light (vec4 Cs, vec4 Cb)
{
  vec3 B = vec3 (hard_light (Cs.r, Cb.r),
                 hard_light (Cs.g, Cb.g),
                 hard_light (Cs.b, Cb.b));
  return composite (Cs, Cb, B);
}

float
soft_light (float source, float backdrop)
{
  float db;

  if (backdrop <= 0.25)
    db = ((16.0 * backdrop - 12.0) * backdrop + 4.0) * backdrop;
  else
    db = sqrt (backdrop);

  if (source <= 0.5)
    return backdrop - (1.0 - 2.0 * source) * backdrop * (1.0 - backdrop);
  else
    return backdrop + (2.0 * source - 1.0) * (db - backdrop);
}

vec4
soft_light (vec4 Cs, vec4 Cb)
{
  vec3 B = vec3 (soft_light (Cs.r, Cb.r),
                 soft_light (Cs.g, Cb.g),
                 soft_light (Cs.b, Cb.b));
  return composite (Cs, Cb, B);
}

vec4
overlay (vec4 Cs, vec4 Cb)
{
  vec3 B = vec3 (hard_light (Cb.r, Cs.r),
                 hard_light (Cb.g, Cs.g),
                 hard_light (Cb.b, Cs.b));
  return composite (Cs, Cb, B);
}

vec4
darken (vec4 Cs, vec4 Cb)
{
  vec3 B = min (Cs.rgb, Cb.rgb);
  return composite (Cs, Cb, B);
}

vec4
lighten (vec4 Cs, vec4 Cb)
{
  vec3 B = max (Cs.rgb, Cb.rgb);
  return composite (Cs, Cb, B);
}

float
color_dodge (float source, float backdrop)
{
  return (source == 1.0) ? source : min (backdrop / (1.0 - source), 1.0);
}

vec4
color_dodge (vec4 Cs, vec4 Cb)
{
  vec3 B = vec3 (color_dodge (Cs.r, Cb.r),
                 color_dodge (Cs.g, Cb.g),
                 color_dodge (Cs.b, Cb.b));
  return composite (Cs, Cb, B);
}


float
color_burn (float source, float backdrop)
{
  return (source == 0.0) ? source : max ((1.0 - ((1.0 - backdrop) / source)), 0.0);
}

vec4
color_burn (vec4 Cs, vec4 Cb)
{
  vec3 B = vec3 (color_burn (Cs.r, Cb.r),
                 color_burn (Cs.g, Cb.g),
                 color_burn (Cs.b, Cb.b));
  return composite (Cs, Cb, B);
}

vec4
exclusion (vec4 Cs, vec4 Cb)
{
  vec3 B = Cb.rgb + Cs.rgb - 2.0 * Cb.rgb * Cs.rgb;
  return composite (Cs, Cb, B);
}

float
lum (vec3 c)
{
  return 0.3 * c.r + 0.59 * c.g + 0.11 * c.b;
}

vec3
clip_color (vec3 c)
{
  float l = lum (c);
  float n = min (c.r, min (c.g, c.b));
  float x = max (c.r, max (c.g, c.b));
  if (n < 0.0) c = l + (((c - l) * l) / (l - n));
  if (x > 1.0) c = l + (((c - l) * (1.0 - l)) / (x - l));
  return c;
}

vec3
set_lum (vec3 c, float l)
{
  float d = l - lum (c);
  return clip_color (vec3 (c.r + d, c.g + d, c.b + d));
}

float
sat (vec3 c)
{
  return max (c.r, max (c.g, c.b)) - min (c.r, min (c.g, c.b));
}

vec3
set_sat (vec3 c, float s)
{
  float cmin = min (c.r, min (c.g, c.b));
  float cmax = max (c.r, max (c.g, c.b));
  vec3 res;

  if (cmax == cmin)
    res = vec3 (0, 0, 0);
  else
    {
      if (c.r == cmax)
        {
          if (c.g == cmin)
            {
              res.b = ((c.b - cmin) * s) / (cmax - cmin);
              res.g = 0.0;
            }
          else
            {
              res.g = ((c.g - cmin) * s) / (cmax - cmin);
              res.b = 0.0;
            }
          res.r = s;
        }
      else if (c.g == cmax)
        {
          if (c.r == cmin)
            {
              res.b = ((c.b - cmin) * s) / (cmax - cmin);
              res.r = 0.0;
            }
          else
            {
              res.r = ((c.r - cmin) * s) / (cmax - cmin);
              res.b = 0.0;
            }
          res.g = s;
        }
      else
        {
          if (c.r == cmin)
            {
              res.g = ((c.g - cmin) * s) / (cmax - cmin);
              res.r = 0.0;
            }
          else
            {
              res.r = ((c.r - cmin) * s) / (cmax - cmin);
              res.g = 0.0;
            }
          res.b = s;
        }
    }
  return res;
}

vec4
color (vec4 Cs, vec4 Cb)
{
  vec3 B = set_lum (Cs.rgb, lum (Cb.rgb));
  return composite (Cs, Cb, B);
}

vec4
hue (vec4 Cs, vec4 Cb)
{
  vec3 B = set_lum (set_sat (Cs.rgb, sat (Cb.rgb)), lum (Cb.rgb));
  return composite (Cs, Cb, B);
}

vec4
saturation (vec4 Cs, vec4 Cb)
{
  vec3 B = set_lum (set_sat (Cb.rgb, sat (Cs.rgb)), lum (Cb.rgb));
  return composite (Cs, Cb, B);
}

vec4
luminosity (vec4 Cs, vec4 Cb)
{
  vec3 B = set_lum (Cb.rgb, lum (Cs.rgb));
  return composite (Cs, Cb, B);
}

void main() {
  vec4 bottom_color = GskTexture(u_source, vUv);
  vec4 top_color = GskTexture(u_source2, vUv);

  vec4 result;
  if (u_mode == 0)
    result = normal(top_color, bottom_color);
  else if (u_mode == 1)
    result = multiply(top_color, bottom_color);
  else if (u_mode == 2)
    result = screen(top_color, bottom_color);
  else if (u_mode == 3)
    result = overlay(top_color, bottom_color);
  else if (u_mode == 4)
    result = darken(top_color, bottom_color);
  else if (u_mode == 5)
    result = lighten(top_color, bottom_color);
  else if (u_mode == 6)
    result = color_dodge(top_color, bottom_color);
  else if (u_mode == 7)
    result = color_burn(top_color, bottom_color);
  else if (u_mode == 8)
    result = hard_light(top_color, bottom_color);
  else if (u_mode == 9)
    result = soft_light(top_color, bottom_color);
  else if (u_mode == 10)
    result = difference(top_color, bottom_color);
  else if (u_mode == 11)
    result = exclusion(top_color, bottom_color);
  else if (u_mode == 12)
    result = color(top_color, bottom_color);
  else if (u_mode == 13)
    result = hue(top_color, bottom_color);
  else if (u_mode == 14)
    result = saturation(top_color, bottom_color);
  else if (u_mode == 15)
    result = luminosity(top_color, bottom_color);
  else
    discard;

  gskSetScaledOutputColor(result, u_alpha);
}

